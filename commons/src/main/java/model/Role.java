package model;

public enum Role {

	ADMIN("admin"), USER("user");

	private String id;

	private Role(String id) {
		this.id = id;
	}
}
