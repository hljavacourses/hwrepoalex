package model;

import lombok.Data;
import lombok.NonNull;

@Data
public class User implements TransferObject {

	private static final long serialVersionUID = 1L;

	private Long id;

	@NonNull
	private String userName;

	@NonNull
	private String city;
	
	public User() {

	}

	public User(Long id, @NonNull String name, @NonNull String city) {
		this.id = id;
		this.userName = name;
		this.city = city;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", city=" + city + "]";
	}
	
	

}
