package jndi;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketAddress;

public class JndiConnect {

	Socket socket;
	ObjectOutputStream oos;
	ObjectInputStream ois;
	SocketAddress address;

	int port;

	public JndiConnect(int port) {
		this.port = port;
	}

	public static void main(String[] args) {
		JndiConnect jndi = new JndiConnect(7777);

		// jndi.bind("ldap", 8001);
		System.out.println(jndi.lookup("ldap"));
	}

	public void bind(String name, Object obj) {

		try {
			resetConnection();

			oos.writeObject("bind");
			oos.writeObject(name);
			oos.writeObject(obj);

			System.out.println(name + " added to registry");
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Object lookup(String name) {
		Object result = null;
		try {
			resetConnection();

			oos.writeObject("lookup");
			oos.writeObject(name);

			result = ois.readObject();
			System.out.println("found " + result);
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private void resetConnection() {
		try {
			socket = new Socket("localhost", port);
			oos = new ObjectOutputStream(socket.getOutputStream());
			ois = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
