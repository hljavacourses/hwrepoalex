package commands;

import java.rmi.RemoteException;
import java.util.List;

import model.TransferObject;
import model.User;

public interface ReadCommand extends Command{

	List<User> read() throws RemoteException;
}
