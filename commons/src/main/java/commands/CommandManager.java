package commands;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

import model.TransferObject;

public interface CommandManager extends Remote, Serializable{
	
	<D extends TransferObject> D execute(final Command command, D obj) throws RemoteException; 

}
