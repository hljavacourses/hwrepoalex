package commands;

import model.User;

public interface UpdateCommand extends Command{
	User updateUser(User user);
}
