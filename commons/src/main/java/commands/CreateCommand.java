package commands;

import java.rmi.RemoteException;
import java.sql.Connection;

import model.TransferObject;

public interface CreateCommand extends Command{
	
	<D extends TransferObject> D create(D object) throws RemoteException;

}
