package commands;

public interface RemoveCommand extends Command{
	void removeUser(Long id);
}
