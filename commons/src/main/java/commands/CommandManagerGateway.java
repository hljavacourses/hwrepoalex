package commands;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

import jndi.JndiConnect;
import model.Role;
import model.User;

public class CommandManagerGateway {

	private int port;

	public CommandManagerGateway() {
		port = (Integer) new JndiConnect(7777).lookup("manager");
	}

	public User save(User user) {
		return (User) sendCommand(CreateCommand.class, user);
	}

	public User update(User user) {
		return (User) sendCommand(UpdateCommand.class, user);
	}

	public User delete(Long id) {
		return (User) sendCommand(RemoveCommand.class, id);
	}

	public List<User> readAll() {
		return (List<User>) sendCommand(ReadCommand.class, null);
	}

	private <D extends Command> Object sendCommand(Class<D> command, Object user) {
		Object result = null;
		try {
			Socket socket = new Socket("localhost", port);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());

			oos.writeObject(command);
			oos.writeObject(user);

			result = ois.readObject();
			System.out.println("Command executed");
			socket.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		return result;
	}

}
