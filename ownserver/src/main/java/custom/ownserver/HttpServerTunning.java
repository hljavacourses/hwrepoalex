package custom.ownserver;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import com.sun.net.httpserver.*;

import commands.Command;
import commands.CreateCommand;
import commands.ReadCommand;
import model.User;

@SuppressWarnings("restriction")
public class HttpServerTunning {

	public static void createCreateHandler(HttpExchange httpExchange) {
		try {
			httpExchange.sendResponseHeaders(200, 0);// response code
			System.out.println("here");

			ObjectInputStream inputStream = new ObjectInputStream(httpExchange.getRequestBody());
			User userToCreate = (User)inputStream.readObject();
			User user = (User) sendCommandToManager(CreateCommand.class, userToCreate);

			ObjectOutputStream os = new ObjectOutputStream(httpExchange.getResponseBody());
			os.writeObject(user);
			os.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public static void createReadAllHandler(HttpExchange httpExchange) {
		try {
			httpExchange.sendResponseHeaders(200, 0);// response code
			
			List<User> users = (List<User>) sendCommandToManager(ReadCommand.class, null);
			
			ObjectOutputStream os = new ObjectOutputStream(httpExchange.getResponseBody());
			os.writeObject(users);
			os.close();
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	private static <D extends Command> Object sendCommandToManager(Class<D> command, User user) {
		Object result = null;
		try {
			URL url = new URL("http://localhost:7700/commands");
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");

			con.setDoOutput(true);
			con.setDoInput(true);

			ObjectOutputStream outputStream = new ObjectOutputStream(con.getOutputStream());
			outputStream.writeObject(command);
			outputStream.writeObject(user);

			result =  new ObjectInputStream(con.getInputStream()).readObject();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
}
