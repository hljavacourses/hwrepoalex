package custom.ownserver;

import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpServer;

import injector.injector.Injector;
import manager.CreateCommandManager;
import manager.ReadCommandManager;

@SuppressWarnings("restriction")
public class MainServerStart {

	public static void main(String[] args) throws Exception {
		HttpServer httpServer = HttpServer.create(new InetSocketAddress(7000), 0);
		httpServer.start();
		Injector.initializeContexsts(httpServer);
		System.out.println("Main server started on port 7000");
	}
}
