import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import commands.CreateCommand;
import model.User;

public class CheckStart {
	
	public static void main(String[] args) throws Exception {
		URL url = new URL("http://localhost:7700/commands");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");

		con.setDoOutput(true);

		ObjectOutputStream outputStream = new ObjectOutputStream(con.getOutputStream());
		outputStream.writeObject(CreateCommand.class);
		outputStream.writeObject(new User(null, "Petya", "City"));
		
		ObjectInputStream input = new ObjectInputStream(con.getInputStream());
		input.readObject();
		
		con.disconnect();
	}

}
