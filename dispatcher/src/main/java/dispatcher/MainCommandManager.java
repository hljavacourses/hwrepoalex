package dispatcher;

import java.rmi.RemoteException;
import commands.Command;
import commands.CreateCommand;
import commands.ReadCommand;
import commands.RemoveCommand;
import commands.UpdateCommand;
import jndi.JndiConnect;
import model.User;

public class MainCommandManager {


	public Object processCommand(Class<Command> commandClass, Object object) throws RemoteException {
		JndiConnect register = new JndiConnect(7777);
		
		Object result = null;
		
		try {
			Command actualCommand = (Command) register.lookup(commandClass.getName());
			
			if(actualCommand.getClass().isInstance(ReadCommand.class)) {
				ReadCommand readCommand = (ReadCommand) actualCommand;
				result = readCommand.read();
			}else if(actualCommand.getClass().isInstance(CreateCommand.class)) {
				CreateCommand createCommand = (CreateCommand) actualCommand;
				result = createCommand.create((User) object);
			}else if(actualCommand.getClass().isInstance(RemoveCommand.class)) {
				RemoveCommand removeCommand = (RemoveCommand) actualCommand;
				removeCommand.removeUser((Long) object);
				result = new User((Long) object, null, null);
			}else if(actualCommand.getClass().isInstance(UpdateCommand.class)) {
				UpdateCommand updateCommand = (UpdateCommand) actualCommand;
				result = updateCommand.updateUser((User) object);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

}
