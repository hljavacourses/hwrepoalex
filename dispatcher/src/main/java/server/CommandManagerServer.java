package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.sun.net.httpserver.*;

import commands.Command;
import commands.CreateCommand;
import commands.ReadCommand;
import dispatcher.MainCommandManager;
import model.User;

@SuppressWarnings("restriction")
public class CommandManagerServer {

	private static final int PORT = 7700;
	private static MainCommandManager manager = new MainCommandManager();

	public void startServer() {
		try {
			ServerSocket server = new ServerSocket(PORT);
			System.out.println("Server started");

			while (true) {
				Socket socket = server.accept();

				ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
				ObjectOutputStream outStream = new ObjectOutputStream(socket.getOutputStream());
				
				Class<Command> clazz = (Class<Command>) inputStream.readObject();
				Object obj = inputStream.readObject();
				
				Object result = manager.processCommand(clazz, obj);
				
				outStream.writeObject(result);
				System.out.println("Command performed");
			}

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		} 
	}

}
