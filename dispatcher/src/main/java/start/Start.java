package start;

import server.CommandManagerServer;

public class Start {
	
	public static void main(String[] args) throws Exception {
		CommandManagerServer server = new CommandManagerServer();
		server.startServer();
	}

}
