package remote.registry;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.Connection;
import java.sql.DriverManager;

import command.CreateUserCommand;
import command.ReadAllUsersCommand;
import commands.CreateCommand;
import commands.ReadCommand;

public class RemoteRegistry {
	public static void main(String[] args) throws Exception {
		Registry registry = LocateRegistry.createRegistry(2007);

		CreateUserCommand createCommand = new CreateUserCommand(getConnectionToMySql());
		ReadAllUsersCommand readAll = new ReadAllUsersCommand(getConnectionToMySql());
		registry.bind(CreateCommand.class.getName(), createCommand);
		registry.bind(ReadCommand.class.getName(), readAll);

		System.out.println("Registry created on port 2007");
	}
	
	private static Connection getConnectionToMySql() {
		Connection connection = null;
		try {
			return DriverManager.getConnection("jdbc:mysql://localhost:3306/users", "student", "student");
		} catch (Exception e) {
			System.out.println(e);
		}

		return connection;
	}
}
