package jndi;

import org.apache.log4j.BasicConfigurator;

import org.jnp.server.Main;

import org.jnp.server.NamingBeanImpl;

import javax.naming.InitialContext;

import javax.naming.NamingException;

import java.io.IOException;

import java.net.InetAddress;

import java.util.Hashtable;

import java.util.Properties;

import static javax.naming.Context.INITIAL_CONTEXT_FACTORY;

import static javax.naming.Context.PROVIDER_URL;

/**
 * 
 * JNDI server for object transfer
 * 
 */
public class JndiServer {

	private static final String CONFIG_PROPERTIES = "jndi.properties";
	private static final String JNDI_URL_PATH = "jndi.url";
	private static final String JNDI_PORT_PATH = "jndi.port";
	private static final String CONTEXT_FACTORY_CLASS_PATH = "jndi.factory.class";
	private static final String RUNTIME_EXCEPTION_MSG = "Unexpected exception in JNDI server";

	private static NamingBeanImpl namingServer;
	private static Main jnpServer;

	private static final Properties PROPERTIES = new Properties();

	// Log4J basic configuration and properties set up
	static {
		BasicConfigurator.configure();
		try {
			PROPERTIES.load(JndiServer.class.getClassLoader().getResourceAsStream(CONFIG_PROPERTIES));

			// we have to initialize context factory in system env to get everything work
			System.setProperty(INITIAL_CONTEXT_FACTORY, getFactoryClassPath());

		} catch (IOException e) {
			throw new RuntimeException(RUNTIME_EXCEPTION_MSG);
		}

	}

	/**
	 * 
	 * @return URL for JNDI from {@see remoteconfig.properties}
	 * 
	 */
	private static String getURL() throws IOException {
		return PROPERTIES.getProperty(JNDI_URL_PATH);
	}

	/**
	 * 
	 * @return port for JNDI from {@see remoteconfig.properties}
	 * 
	 */
	private static String getPort() throws IOException {
		return PROPERTIES.getProperty(JNDI_PORT_PATH);
	}

	/**
	 * 
	 * @return class path for JNDI factory from {@see remoteconfig.properties}
	 * 
	 */
	private static String getFactoryClassPath() throws IOException {
		return PROPERTIES.getProperty(CONTEXT_FACTORY_CLASS_PATH);
	}

	/**
	 * 
	 * @return configuired {@link InitialContext}
	 * 
	 */
	private static InitialContext getContext() throws IOException, NamingException {

		Hashtable<String, String> jndiProperties = new Hashtable<>();
		jndiProperties.put(INITIAL_CONTEXT_FACTORY, getFactoryClassPath());
		jndiProperties.put(PROVIDER_URL, getURL() + ":" + getPort());

		return new InitialContext(jndiProperties);
	}

	/**
	 * 
	 * Binds an object to {@link InitialContext}
	 * 
	 */
	public static void bindJndi(String key, Object obj) throws NamingException, IOException {
		getContext().rebind(key, obj);
	}

	/**
	 * 
	 * @return {@link Object} from {@link InitialContext} by a key
	 * 
	 */
	public static Object lookupJndi(String key) throws NamingException, IOException {
		return getContext().lookup(key);
	}

	/**
	 * 
	 * Starts JNDI server
	 * 
	 */
	public static void start() {
		namingServer = new NamingBeanImpl();
		jnpServer = new Main();

		try {
			namingServer.start();
			jnpServer.setNamingInfo(namingServer);
			jnpServer.setBindAddress(InetAddress.getLocalHost().getHostName());
			jnpServer.start();
		} catch (Exception e) {
			jnpServer.stop();
			namingServer.stop();
			throw new RuntimeException(RUNTIME_EXCEPTION_MSG);
		}
	}

	/**
	 * 
	 * Stops JNDI server
	 * 
	 */
	public static void stop() {
		if (jnpServer != null && namingServer != null) {
			jnpServer.stop();
			namingServer.stop();
		}
	}

	public static void main(String[] args) {
		JndiServer.start();
	}

}