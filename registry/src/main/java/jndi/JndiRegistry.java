package jndi;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class JndiRegistry {

	private static final int PORT = 7777;

	private Map<String, Object> storage = new HashMap<>();

	private ServerSocket server;

	JndiRegistry() {

	}

	public static void main(String[] args) {
		new JndiRegistry().startServer();
	}

	public void startServer() {
		try {
			server = new ServerSocket(PORT);
			System.out.println("Server started");

			while (true) {
				Socket socket = server.accept();

				ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
				ObjectOutputStream outStream = new ObjectOutputStream(socket.getOutputStream());

				String type = (String) inputStream.readObject();

				if ("bind".equals(type)) {
					String name = (String) inputStream.readObject();
					Object obj = inputStream.readObject();
					bind(name, obj);
					System.out.println(String.format("Binded %s with name %s.", obj, name));
				} else {
					String name = (String) inputStream.readObject();
					Object object = lookup(name);
					outStream.writeObject(object);
					System.out.println("Sent " + name);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	void stopServer() {
		try {
			server.close();
			System.out.println("Server stopped");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void bind(String name, Object obj) {
		storage.put(name, obj);
	}

	private Object lookup(String name) {
		return storage.get(name);
	}

}
