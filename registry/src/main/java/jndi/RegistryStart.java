package jndi;

import java.io.IOException;

import javax.naming.NamingException;

public class RegistryStart {

	public static void main(String[] args) {
		JndiServer.start();

		try {
			JndiServer.bindJndi("ldapPort", 8001);
			System.out.println(JndiServer.lookupJndi("ldapPort"));
		} catch (NamingException | IOException e) {
			e.printStackTrace();
		}
	}

}
