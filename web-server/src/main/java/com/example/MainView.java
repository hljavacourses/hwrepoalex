package com.example;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;

import commands.CommandManagerGateway;
import model.User;

@Route
@PWA(name = "My TODO App", shortName = "My TODO")
@Push
public class MainView extends HorizontalLayout implements BeforeEnterObserver {

	@Autowired
	private CommandManagerGateway service;

	public MainView() {
		add(createDetailsTab(), createListTab());
	}

	private VerticalLayout createListTab() {
		Grid<User> users = new Grid<>(User.class);
		users.setMaxWidth("500px");
		Button readButton = new Button("Read");

		readButton.addClickListener(event -> {
			users.setItems(service.readAll());
		});

		return new VerticalLayout(users, readButton);

	}

	private VerticalLayout createDetailsTab() {

		// labels
		Label nameLabel = new Label("Customer Name");
		Label idLabel = new Label("Customer ID");
		Label adressLabel = new Label("Customer Adress");

		// text fields
		TextField nameField = new TextField();
		TextField idField = new TextField();
		TextField adressField = new TextField();

		// fields
		HorizontalLayout customerName = new HorizontalLayout(nameLabel, nameField);
		HorizontalLayout customerId = new HorizontalLayout(idLabel, idField);
		HorizontalLayout customerAdress = new HorizontalLayout(adressLabel, adressField);
		VerticalLayout fields = new VerticalLayout(customerName, customerId, customerAdress);
		fields.setJustifyContentMode(JustifyContentMode.CENTER);
		fields.setSizeFull();

		TextArea log = new TextArea();
		log.setWidth("350px");
		log.setReadOnly(true);

		// buttons
		Button addButton = new Button("Add Customer");
		addButton.addClickListener(event -> {
			User user = new User(idField.getValue() == null ? Long.valueOf(idField.getValue()) : null,
					nameField.getValue(), adressField.getValue());
			User createdUser = service.save(user);
			log.setValue("Created - " + createdUser.toString());

		});

		Button updateButton = new Button("Update Customer");
		updateButton.addClickListener(event -> {
			User user = new User(Long.valueOf(idField.getValue()), nameField.getValue(), adressField.getValue());
			User updatedUser = service.update(user);
			log.setValue("Updated - " + updatedUser.toString());
		});

		Button deleteButton = new Button("Delete Customer");
		updateButton.addClickListener(event -> {
			Long id = Long.valueOf(idField.getValue());
			service.delete(id);
			log.setValue("Removed user with ID " + id);

		});

		HorizontalLayout firstButtonRow = new HorizontalLayout(addButton, updateButton);
		HorizontalLayout secondButtonRow = new HorizontalLayout(deleteButton);

		VerticalLayout buttons = new VerticalLayout(firstButtonRow, secondButtonRow);

		return new VerticalLayout(fields, buttons, log);
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		if (!"passed".equals(event.getUI().getSession().getAttribute("auth"))) {
			event.forwardTo("login");
		}
	}

}
