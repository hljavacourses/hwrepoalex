package injector.injector;

import java.sql.Connection;
import java.sql.DriverManager;

import jndi.JndiConnect;

public class Binder {

	private static final String DB_URL = "jdbc:postgresql://127.0.0.1:5432/StockMarket";
	private static final String USER = "username";
	private static final String PASS = "password";

	public static void main(String[] args) {

		JndiConnect connection = new JndiConnect(7777);
		connection.bind("ldap", 8001);
		connection.bind("manager", 7700);

		Connection dbConnection = getConnectionToDb();
	}

	
	private static Connection getConnectionToDb() {
		Connection connection = null;
		try {
			return DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (Exception e) {
			System.out.println(e);
		}

		return connection;
	}
}
