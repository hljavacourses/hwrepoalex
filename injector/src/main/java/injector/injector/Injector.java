package injector.injector;

import java.lang.reflect.Field;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Set;

import org.reflections.Reflections;
import org.reflections.scanners.FieldAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import com.sun.net.httpserver.HttpServer;

import annotations.InjectRmi;
import commands.Command;
import commands.CreateCommand;
import commands.ReadCommand;
import manager.CreateCommandManager;
import manager.ReadCommandManager;

@SuppressWarnings("restriction")
public class Injector {

	public static void main(String[] args) {
		Reflections reflections = new Reflections(new ConfigurationBuilder()
				.setUrls(ClasspathHelper.forPackage("command")).setScanners(new FieldAnnotationsScanner()));
		System.out.println(reflections.getFieldsAnnotatedWith(InjectRmi.class));
	}

	public static void initializeContexsts(HttpServer httpServer) {
		try {
			Registry register = LocateRegistry.getRegistry(2007);
			Reflections reflections = new Reflections(new ConfigurationBuilder()
					.setUrls(ClasspathHelper.forPackage("command")).setScanners(new FieldAnnotationsScanner()));
			Set<Field> fields = reflections.getFieldsAnnotatedWith(InjectRmi.class);
			
			System.out.println(fields);

			for (Field field : fields) {
				if (field.getType().equals(ReadCommand.class)) {
					ReadCommand command = (ReadCommand) register.lookup(field.getType().getName());
					ReadCommandManager manager = new ReadCommandManager();
					Field commandField = manager.getClass().getDeclaredField("command");
					commandField.setAccessible(true);
					commandField.set(manager, command);
					
					System.out.println("read");
					httpServer.createContext("/read", manager);
					
				} else if (field.getType().equals(CreateCommand.class)) {
					CreateCommand command = (CreateCommand) register.lookup(field.getType().getName());
					CreateCommandManager manager = new CreateCommandManager();
					Field commandField = manager.getClass().getDeclaredField("command");
					commandField.setAccessible(true);
					commandField.set(manager, command);
					
					System.out.println("create");
					httpServer.createContext("/create", manager);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
