package service;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.concurrent.ExecutionException;

import commands.CreateCommand;
import model.User;

public class CrudServiceImpl implements CrudService {

	@Override
	public void remove(Long id) throws RemoteException, NotBoundException, ExecutionException, InterruptedException {
		// TODO Auto-generated method stub

	}

	@Override
	public User create(User user) throws RemoteException, NotBoundException, ExecutionException, InterruptedException {
		User result = null;
		try {
			URL url = new URL("http://localhost:7000/create");
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");

			con.setDoOutput(true);
			con.setDoInput(true);

			ObjectOutputStream outputStream = new ObjectOutputStream(con.getOutputStream());
			outputStream.writeObject(user);

			result = (User) new ObjectInputStream(con.getInputStream()).readObject();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;

	}

	@Override
	public User find(Long id) throws RemoteException, NotBoundException, ExecutionException, InterruptedException {
		return null;
		// TODO Auto-generated method stub

	}

	@Override
	public User update(User user) throws RemoteException, NotBoundException, ExecutionException, InterruptedException {
		return user;
		// TODO Auto-generated method stub

	}

}
