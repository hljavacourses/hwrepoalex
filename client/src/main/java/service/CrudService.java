package service;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.concurrent.ExecutionException;

import model.User;

public interface CrudService {

	void remove(final Long id) throws RemoteException, NotBoundException, ExecutionException, InterruptedException;

	User create(final User user) throws RemoteException, NotBoundException, ExecutionException, InterruptedException;

	User find(final Long id) throws RemoteException, NotBoundException, ExecutionException, InterruptedException;

	User update(final User user) throws RemoteException, NotBoundException, ExecutionException, InterruptedException;
}
