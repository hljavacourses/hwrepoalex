package action;

import java.awt.event.ActionEvent;

import controller.UserController;
import lombok.Setter;
import view.button.ListTableActionListener;

public class DeleteButtonActionListener extends ListTableActionListener {

	@Setter
	private UserController controller;

	public void actionPerformed(ActionEvent e) {
		if (table.getSelectedRow() == -1) {
			return;
		}
		Long id = Long.class.cast(table.getValueAt(table.getSelectedRow(), 0));
		controller.removeUser(id);
		table.revalidate();
	}
}