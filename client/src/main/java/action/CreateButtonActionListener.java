package action;

import java.awt.event.ActionEvent;

import controller.UserController;
import lombok.Setter;
import model.User;
import view.button.ListTableActionListener;

public class CreateButtonActionListener extends ListTableActionListener {

	@Setter
	private UserController controller;

	public void actionPerformed(ActionEvent e) {
		User createdUser = controller.createUser(new User(null, "Lexa", "Minsk"));
		list.add(createdUser);
		table.revalidate();
	}

}
