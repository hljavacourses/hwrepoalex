package view.button;

import lombok.Setter;
import model.User;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.util.List;

public abstract class ListTableActionListener implements ActionListener {

	@Setter
	protected JTable table;
	@Setter
	protected List<User> list;

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public List<User> getList() {
		return list;
	}

	public void setList(List<User> list) {
		this.list = list;
	}

}
