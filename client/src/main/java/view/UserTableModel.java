package view;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import controller.UserController;
import model.User;

public class UserTableModel extends AbstractTableModel {

	List<String> columns;

	List<User> usersList;

	private UserController controller;

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	public List<String> getColumns() {
		return columns;
	}

	public void setColumns(List<String> columns) {
		this.columns = columns;
	}

	public List<User> getUsersList() {
		return usersList;
	}

	public void setUsersList(List<User> usersList) {
		this.usersList = usersList;
	}

	public UserController getController() {
		return controller;
	}

	public void setController(UserController controller) {
		this.controller = controller;
	}

	@Override
	public int getColumnCount() {
		return columns.size();
	}

	@Override
	public String getColumnName(int column) {
		return columns.get(column);
	}

	@Override
	public int getRowCount() {
		return usersList.size();
	}

	@Override
	public void setValueAt(final Object value, final int rowIndex, final int columnIndex) {
		User user = usersList.get(rowIndex);
		switch (columnIndex) {
		case 0:
			user.setId(Long.class.cast(value));
			break;
		case 1:
			user.setUserName(value.toString());
			break;
		case 2:
			user.setCity(value.toString());
			break;
		}
		controller.updateUser(user);
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		User user = usersList.get(rowIndex);
		Object value = null;
		switch (columnIndex) {
		case 0:
			value = user.getId();
			break;
		case 1:
			value = user.getUserName();
			break;
		case 2:
			value = user.getCity();
			break;
		}
		return value;
	}
}
