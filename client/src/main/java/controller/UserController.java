package controller;

import lombok.Setter;
import model.User;
import service.CrudService;

public class UserController {

	@Setter
	private CrudService service;

	public User createUser(User user) {
		try {
			return service.create(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public void removeUser(Long id) {

	}

	public void updateUser(User user) {

	}

	public void readUser(Long id) {

	}

}
