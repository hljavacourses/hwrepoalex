package ldap.setup;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

public class Lookuper {

	public void doLookup() {
		Properties properties = new Properties();
		properties.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		properties.put(Context.PROVIDER_URL, "ldap://localhost:8001");
		properties.put(Context.SECURITY_AUTHENTICATION, "simple");
		properties.put(Context.SECURITY_PRINCIPAL, "uid=admin,ou=system");
		properties.put(Context.SECURITY_CREDENTIALS, "secret");
		try {
			DirContext context = new InitialDirContext(properties);
			System.out.println("connected");
			Attributes attrs = context.getAttributes("uid=admin, cn=users, dc=iba, dc=by");
			System.out.println("Login: " + attrs.get("uid").get());
			System.out.println("Password: " + attrs.get("userpassword").get());
			System.out.println("Pass: " + attrs.get("cn").get());
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		Lookuper sample = new Lookuper();
		sample.doLookup();
	}

}
