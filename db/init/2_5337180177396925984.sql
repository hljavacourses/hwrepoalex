--  StockMarket database creation script
--  This script was created for use with Derby
-- 
--  To use this script to build/re-create the database, do the
--  following:
--
--  1. Start the Derby Database Server
--  2. Create the StockMarket database using public as username and password
--  3. Populate the database:		
--	Go to the terminal window and type the following commands:
--   		C:\Sun\AppServer\derby>java -cp lib\derbyclient.jar;lib\derbytools.jar org.apache.derby.tools.ij
--		ij version 10.1
--		ij> connect 'jdbc:derby://localhost:1527/StockMarket;user=public;password=public';
--		ij> run 'C:\student\resources\brokertool\StockMarket.sql';
--		
--
--    
--  Database name: StockMarket
-- 
--     Table name: Customer
-- 		   ssn		char (15)	
-- 		   cust_name	char (40)
-- 		   address	char (100)
-- 
--     Table name: Shares
-- 		   ssn		char (15)
-- 		   symbol	char (8)
-- 		   quantity	int
-- 
--     Table name: Stock
-- 		   symbol	char (8)	
-- 		   price	real
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- 
--  Table structure for table 
-- 
CREATE TABLE Customer (
	ssn CHAR(15) NOT NULL,
	cust_name CHAR(40),
	address CHAR(100)
);

CREATE UNIQUE INDEX idx1 ON Customer (ssn);

CREATE TABLE Shares (
	ssn CHAR(15) NOT NULL,
	symbol CHAR(8) NOT NULL,
	quantity INT
);

CREATE TABLE Stock (
	symbol CHAR(15) NOT NULL,
	price real
);

CREATE UNIQUE INDEX idx2 ON Stock (symbol);

-- 
--  Initial "test" data
-- 

INSERT INTO Customer VALUES ('111-11-1111', 'Test Customer', '2222 Easy Street, West Beach AZ');

INSERT INTO Customer VALUES ('999-45-9034', 'Asok Perumainar', '1444 England Lane, Broomfield CO');

INSERT INTO Customer VALUES ('999-78-9012', 'Anthony Orapallo', '123 Tea Street, Columbia MD');

INSERT INTO Customer VALUES ('999-90-9009', 'Terri Cubeta', '636 Somewhere Rd, Rosslyn VA');

INSERT INTO Customer VALUES ('999-90-8765', 'Bryan Basham', '3290 Course Way, Broomfield CO');

INSERT INTO Customer VALUES ('999-33-4444', 'Georgianna DG Meagher', '1000 Mother Court, Columbia MD');

INSERT INTO Customer VALUES ('999-44-5555', 'Tom McGinn', '1525 Educator Drive, Burlington MA');

INSERT INTO Shares VALUES ('999-45-9034', 'SUNW', 200);
INSERT INTO Shares VALUES ('999-45-9034', 'DUKE', 1200);
INSERT INTO Shares VALUES ('999-78-9012', 'JSVCo', 120);
INSERT INTO Shares VALUES ('999-90-9009', 'BWInc', 35);
INSERT INTO Shares VALUES ('999-90-8765', 'GMEnt', 200);
INSERT INTO Shares VALUES ('999-90-8765', 'PMLtd', 109);
INSERT INTO Shares VALUES ('999-44-5555', 'TMAs', 135);

INSERT INTO Stock VALUES ('SUNW', 135.0);
INSERT INTO Stock VALUES ('CyAs', 34.0);
INSERT INTO Stock VALUES ('DUKE', 12.375);
INSERT INTO Stock VALUES ('ABStk', 20.875);
INSERT INTO Stock VALUES ('JSVCo', 24.00);
INSERT INTO Stock VALUES ('TMAs', 80.125);
INSERT INTO Stock VALUES ('BWInc', 6.25);
INSERT INTO Stock VALUES ('GMEnt', 52.50);
INSERT INTO Stock VALUES ('PMLtd', 201.875);
INSERT INTO Stock VALUES ('JDK', 13.50);

SELECT * FROM Customer;
SELECT * FROM Shares;
SELECT * FROM Stock;
