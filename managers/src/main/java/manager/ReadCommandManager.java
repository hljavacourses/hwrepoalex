package manager;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

import com.sun.net.httpserver.*;

import annotations.InjectRmi;
import commands.CreateCommand;
import commands.ReadCommand;
import model.User;

public class ReadCommandManager implements HttpHandler{
	
	@InjectRmi
	private ReadCommand command;

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		try {
			exchange.sendResponseHeaders(200, 0);// response code

			List<User> users = (List<User>) command.read();
			
			ObjectOutputStream os = new ObjectOutputStream(exchange.getResponseBody());
			os.writeObject(users);
			os.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			ObjectOutputStream os = new ObjectOutputStream(exchange.getResponseBody());
			os.writeObject(null);
			os.close();
		}
		
	}

}
