package manager;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import annotations.InjectRmi;
import commands.Command;
import commands.CommandManager;
import commands.CreateCommand;
import model.TransferObject;
import model.User;

public class CreateCommandManager implements HttpHandler {
	
	@InjectRmi
	private CreateCommand command;

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		try {
			exchange.sendResponseHeaders(200, 0);// response code

			ObjectInputStream inputStream = new ObjectInputStream(exchange.getRequestBody());
			User userToCreate = (User)inputStream.readObject();
			User user = (User) command.create(userToCreate);

			ObjectOutputStream os = new ObjectOutputStream(exchange.getResponseBody());
			os.writeObject(user);
			os.close();

		} catch (Exception e) {
			e.printStackTrace();
			ObjectOutputStream os = new ObjectOutputStream(exchange.getResponseBody());
			os.writeObject(null);
			os.close();
		}
		
	}


}
