package command;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import commands.ReadCommand;
import model.TransferObject;
import model.User;

public class ReadAllUsersCommand extends UnicastRemoteObject implements ReadCommand {

	private static final long serialVersionUID = 1L;
	
	Connection connection;

	public ReadAllUsersCommand(Connection con) throws RemoteException {
		connection = con;
	}

	public static void main(String[] args) throws RemoteException, SQLException {
		ReadAllUsersCommand command = new ReadAllUsersCommand(
				DriverManager.getConnection("jdbc:mysql://localhost:3306/users", "student", "student"));
		command.closeConnection();

	}
	
	@Override
	public List<User> read() throws RemoteException{
		List<User> result = new ArrayList<>();
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("Select * FROM users");
			while (rs.next()) {
				result.add(new User(rs.getLong(1), rs.getString(2), rs.getString(3)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public void closeConnection() throws RemoteException {
		try {
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
