package command;

import java.sql.Connection;
import java.sql.Statement;

import commands.RemoveCommand;

public class RemoveUserCommand implements RemoveCommand {

	private static final long serialVersionUID = 1L;

	Connection connection;

	public RemoveUserCommand(Connection con) {
		connection = con;
	}

	@Override
	public void removeUser(Long id) {
		try {
			Statement stmt = connection.createStatement();
			stmt.executeUpdate("DELETE * FROM users WHERE id = " + id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
