package command;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import commands.CreateCommand;
import model.TransferObject;
import model.User;

public class CreateUserCommand extends UnicastRemoteObject implements CreateCommand {

	Connection connection;

	public CreateUserCommand(Connection con) throws RemoteException {
		connection = con;
	}

	private static final long serialVersionUID = 1L;

	public static void main(String[] args) throws RemoteException, SQLException {
		CreateUserCommand command = new CreateUserCommand(
				DriverManager.getConnection("jdbc:mysql://localhost:3306/users", "student", "student"));
		command.create(new User(null, "Lexa", "Minsk"));
		command.closeConnection();

	}

	@Override
	public <D extends TransferObject> D create(D object) {

		User resultUser = null;
		try {
			User user = (User) object;
			Statement stmt = connection.createStatement();
			stmt.executeUpdate(String.format("INSERT INTO users " + "VALUES (%d, '%s', '%s')", user.getId(),
					user.getUserName(), user.getCity()));
			ResultSet rs = stmt
					.executeQuery(String.format("Select * from users WHERE name = '%s'", user.getUserName()));
			if (rs.next()) {
				resultUser = new User(rs.getLong(1), rs.getString(2), rs.getString(3));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return (D) resultUser;
	}

	public void closeConnection() throws RemoteException {
		try {
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
