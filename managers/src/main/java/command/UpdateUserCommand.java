package command;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import commands.UpdateCommand;
import model.User;

public class UpdateUserCommand implements UpdateCommand {
	
	private static final long serialVersionUID = 1L;

	Connection connection;

	public UpdateUserCommand(Connection con) {
		connection = con;
	}

	@Override
	public User updateUser(User user) {
		User resultUser = null;
		try {
			Statement stmt = connection.createStatement();
			stmt.executeUpdate(String.format("UPDATE users " + "SET name = '%s', city = '%s' WHERE id = %d",
					user.getUserName(), user.getCity(), user.getId()));
			ResultSet rs = stmt
					.executeQuery(String.format("Select * from users WHERE name = '%s'", user.getUserName()));
			if (rs.next()) {
				resultUser = new User(rs.getLong(1), rs.getString(2), rs.getString(3));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return resultUser;
	}
}
